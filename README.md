# Yellow SEO for Django

## Installation
* Add `'yellow_seo'` to your INSTALLED_APPS in django settings.
* Add `url(r'any_seo_url/', include('yellow_seo.urls'))` to your urls.py file.
* Run `python manage.py syncdb` command to create table.
* Add `{% if user.is_superuser %}<script type="text/javascript" src="{{ STATIC_URL }}js/top_bar.js"></script>{% endif %}` to your template after jQuery.
* Add `{% if user.is_superuser %}<link rel="stylesheet" type="text/css" href="{{ STATIC_URL }}css/bar.css">{% endif %}` to your template.
* Add `{% include "yellow_seo/top_bar.html" %}` to your base template inside `<body>` tag.

## Usage
* Add `{% load seo_tags %}` to use template tags.
* Use template tags:
 * to set title {% seo_title 'default seo_title' %}
 * to set description {% seo_description 'default seo_description' %}
 * to set keywords {% seo_keywords 'default seo_keywords' %}
 * to set seo_text {% seo_text 'default seo_text' %}

## Requirements
* jQuery
* Django version > 1.3
* Hands
* Brain