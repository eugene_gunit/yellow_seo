 #-*- coding: utf-8 -*-
from django.db import models


class Seo(models.Model):
    seo_url = models.URLField(verbose_name=u"Адрес к которому привязан СЕО текст", unique=True)
    seo_title = models.CharField(verbose_name=u'Сео тайтл', max_length=255, blank=True, null=True)
    seo_canonical = models.CharField(verbose_name=u'Сео каноникал', max_length=255, blank=True, null=True)
    seo_text = models.TextField(verbose_name=u'Ceо текст', blank=True, null=True)
    seo_description = models.CharField(verbose_name=u'Сео описание', max_length=255, blank=True, null=True)
    seo_keywords = models.CharField(verbose_name=u'Сео ключевые слова', max_length=255, blank=True, null=True)