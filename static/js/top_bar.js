$(function() {
	$("#seo-form-send").click(function(){
		var current_url = $("#current_url").val();
		var seo_title = $('[name="seo-title"]').val();
		var seo_keywords = $('[name="seo-keywords"]').val();
		var seo_description = $('[name="seo-description"]').val();
		var seo_text = $('[name="seo-text"]').val();
		var seo_canonical = $('[name="seo-canonical"]').val();
		var post_url = $('[name="seo-post-url"]').val();
		$("#seo-form-send").attr("disabled", "disabled");
		$("#seo-form-send").val("Сохранение...");
		$.post(post_url, {"seo_title": seo_title, "seo_canonical": seo_canonical, "seo_keywords": seo_keywords, "seo_description": seo_description, "seo_text": seo_text, "seo_url": current_url}, function(data) {
		 	$("#seo-form-send").removeAttr("disabled");
		 	$("#seo-form-send").val("Сохранить");
		});
	});
});
$(function() {
	$('.jBar').hide();
	$('.jRibbon').show().removeClass('up', 300);
	$('.jTrigger').click(function(){
		$('.jRibbon').toggleClass('up', 300);
		$('.jBar').slideToggle();
	});
});