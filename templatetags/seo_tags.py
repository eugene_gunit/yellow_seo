#-*- coding: utf-8 -*-
from yellow_seo.models import Seo
from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def seo_title(context, default=None):
    url = context['request'].META['PATH_INFO']
    try:
        seo = Seo.objects.get(seo_url=url)
        return seo.seo_title
    except Seo.DoesNotExist:
        if default:
            return default
    return ''


@register.simple_tag(takes_context=True)
def seo_description(context, default=None):
    url = context['request'].META['PATH_INFO']
    try:
        seo = Seo.objects.get(seo_url=url)
        return seo.seo_description
    except Seo.DoesNotExist:
        if default:
            return default
    return ''


@register.simple_tag(takes_context=True)
def seo_keywords(context, default=None):
    url = context['request'].META['PATH_INFO']
    try:
        seo = Seo.objects.get(seo_url=url)
        return seo.seo_keywords
    except Seo.DoesNotExist:
        if default:
            return default
        else:
            return ''


@register.simple_tag(takes_context=True)
def seo_text(context, default=None):
    url = context['request'].META['PATH_INFO']
    try:
        seo = Seo.objects.get(seo_url=url)
        return seo.seo_text
    except Seo.DoesNotExist:
        if default:
            return default
    return ''


@register.simple_tag(takes_context=True)
def seo_canonical(context, flag=True, default=None):
    url = context['request'].META['PATH_INFO']
    try:
        seo = Seo.objects.get(seo_url=url)
        if seo.seo_canonical:
            if flag:
                return '<link rel="canonical" href="%s"/>' % seo.seo_canonical
            else:
                return seo.seo_canonical
        elif default:
            return default
    except Seo.DoesNotExist:
        if default:
            return default
    return ''