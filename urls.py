from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(r'^get/$', 'yellow_seo.views.get_seo', name='get_seo'),
    url(r'^post/$', 'yellow_seo.views.post_seo', name='post_seo'),
)
