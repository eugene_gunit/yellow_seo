#-*- coding: utf-8 -*-
from yellow_seo.models import Seo
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def get_seo(request):
    try:
        seo = Seo.objects.get(seo_url=request.GET['url'])
        ctx = {
            'empty': False,
            'seo_title': seo.seo_title,
            'seo_keywords': seo.seo_keywords,
            'seo_description': seo.seo_description,
            'seo_text': seo.seo_text,
            'seo_canonical': seo.seo_canonical,
        }
    except Seo.DoesNotExist:
        ctx = {'empty': True}
    return render(request, 'yellow_seo/return_get.html', ctx, content_type="application/xhtml+xml")


@csrf_exempt
def post_seo(request):
    if request.method == "POST":
        seo, created = Seo.objects.get_or_create(seo_url=request.POST['seo_url'])
        seo.seo_url = request.POST.get('seo_url', '')
        seo.seo_title = request.POST.get('seo_title', '')
        seo.seo_description = request.POST.get('seo_description', '')
        seo.seo_keywords = request.POST.get('seo_keywords', '')
        seo.seo_text = request.POST.get('seo_text', '')
        seo.seo_canonical = request.POST.get('seo_canonical', '')
        seo.save()
        status = 'succes'
    else:
        status = 'failed'
    return render_to_response('yellow_seo/return_post.html', {"status": status},
                              context_instance=RequestContext(request))